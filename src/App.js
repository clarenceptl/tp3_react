import './App.css';
import { useState,useEffect } from 'react';
import AfficheApi from './components/AfficherApi';
import WelcomePage from './components/WelcomePage';
import Route from "./components/Route";
import Meal from "./components/Meal";
import Drink from "./components/Drink";
import Header from "./components/Header";
import ApiUsed from "./components/ApiUsed";
 /**
  * Ici a la base je voulais que le composant affiche api charge les apis et passe la data à app.js(Child -> Parent) puis après va dans la page Meal ou Drink (Child -> parent).
  * Mais je n'ai pas réussi à régler le problème de : si on refresh après sur la page Meal ou Drink -> plus aucune data.
  * Or si on passe la data du parent à l'enfant, cela fonctionne.
  */
function App() {
  let [data, setData] = useState([]); //meal data
  let [data2,setData2] = useState([]); //drink data
  let allData = [];
  /**
   * call api 1 : meal 
   */
  useEffect(() => {
    let mounted = true;
    fetch('https://www.themealdb.com/api/json/v1/1/random.php')
    .then(response => response.json())
    .then(data => {
      if(mounted === true){
      setData(data.meals[0]);
      }
    })
    return () => mounted = false;
  },[]);
  /**
   * call api 2 : drink 
   */

  useEffect(() => {
    let mounted2 = true;
    fetch('https://www.thecocktaildb.com/api/json/v1/1/random.php')
      .then(response => response.json())
      .then(data => {
      if(mounted2 === true){
        setData2(data.drinks[0]);
      }
    })
    return () => mounted2 = false;
  },[]);
      
  allData.push(data);
  allData.push(data2);

  if(!data || !data2){
    return(
      <></>
    )
  }
  return (
    <div className="App">
      {window.location.pathname === '/' ? null : <Header />}
      <div className="container">
        <Route path="/">
          <WelcomePage />
        </Route>
        <Route path="/home">
          <AfficheApi data={allData}/>
        </Route>
        <Route path="/meal">
          <Meal data={data}/>
        </Route>
        <Route path="/drink">
          <Drink data={data2}/>
        </Route>
        <Route path="/api-use">
          <ApiUsed />
        </Route>
      </div>
    </div>
  ); 
}

export default App;
