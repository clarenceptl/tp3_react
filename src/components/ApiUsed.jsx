export default function ApiUsed() {
    return(
        <div className="container-home">
            <div className="api">
                <h1>Api utilisées :</h1>
                <h2>TheCocktaildb</h2>
                <p><strong>Endpoint :</strong> 'https://www.thecocktaildb.com/api/json/v1/1/random.php'<br/>
                Une api gratuite sans clé d'accès, qui 
                sert à récupérer les information d'un cocktail au hasard avec les détails de la préparation, les ingrédients etc..
                </p>
                <p>
                TheCocktailDB a été créé en 2015 pour fournir une source de données api gratuite pour les boissons en ligne dans l'espoir que les développeurs construisent des applications et des projets sympas sur cette base. TheCocktailDB a été créé sur les forums Kodi pour permettre de parcourir les recettes de cocktails sur votre télévision.
                </p>
                <p>Lien de leur api : https://www.thecocktaildb.com/api.php</p>
                <h2>TheMealdb</h2>
                <p><strong>Endpoint :</strong> 'https://www.themealdb.com/api/json/v1/1/random.php'<br/>
                Une api gratuite sans clé d'accès, qui 
                sert à récupérer les information d'un plat au hasard avec les détails de la préparation, les ingrédients etc..
                </p>
                <p>
                ThemealDB a été créé en 2016 pour fournir une source de données api gratuite pour les boissons en ligne dans l'espoir que les développeurs construisent des applications et des projets sympas sur cette base. TheCocktailDB a été créé sur les forums Kodi pour permettre de parcourir les recettes de cocktails sur votre télévision.
                </p>
                <p>Lien de leur api : https://www.themealdb.com/api.php</p>
            </div>
        </div>
    )   
}