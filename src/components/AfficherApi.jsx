import Link from "./Link";

export default function AfficheApi({data}){

    /*
    affiche les 2 apis, et fait office de home page
    api use https://www.thecocktaildb.com/api.php
    https://www.themealdb.com/api.php
    */
    return (
        <div className="container-home">
            <div className="randomMealandDrink">
                <h1>The perfect drink for the perfect meal</h1>
                <p>Today's menu</p>
                <div className="menu">
                    <div className="repas">
                        <h2>{data[0].strMeal}</h2>
                        <h4>{data[0].strArea} meal.</h4>
                        <img className="images" src={data[0].strMealThumb} alt="meal"/>
                        <Link href="/meal" className="link">
                            check the recipe
                        </Link>
                    </div>
                    <div className="line"></div>
                    <div className="boisson">
                        <h2>{data[1].strDrink}</h2>
                        <h4>Category : {data[1].strCategory}.</h4>
                        <img className="images" src={data[1].strDrinkThumb} alt="drink"/>
                        <Link href="/drink" className="link">
                            check the recipe
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
//{() => setMessage('au revoir')} execute la fonction au click
// {setMessage('au revoir')} execute automatiquement la fonction (pas forcément au click)