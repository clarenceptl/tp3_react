export default function WelcomePage(){
    return (
        <div className="welcome_page">
            <div className="enter">
                <i className="fa fa-angle-double-right"></i>
            </div>
                <div className="click" >
                    <a href="/home">Click here</a>
                </div>
        </div>
    );
}