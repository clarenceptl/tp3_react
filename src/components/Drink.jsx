import { useEffect, useState } from "react";

export default function Drink({data}){
    let [ingredients, setIngredients] =  useState([]); 
    useEffect(() => {
        for (let i = 1;i<=20;i++){
            let valeur1 = data['strIngredient'+i] ?? 'no data';
            let valeur2 = data['strMeasure'+i] ?? 'no data';
            let valeurTot = valeur1+' : '+ valeur2;
            if(valeur1 !== 'no data' && valeur1 !=="" && valeur2 !== 'no data' && valeur2 !==""){
                setIngredients(ingredients => [...ingredients,valeurTot])
            }
        }
    },[data]);
    return (
    <div className="container-meal">
        <div className="randomMealandDrink">
            <h1>{data.strDrink}</h1>
            <h2>{data.strAlcoholic} drink</h2>
            <p>Type : {data.strArea} {data.strCategory}</p>
            <div className="description">
                <img className="images" src={data.strDrinkThumb} alt="drink"/>
                <div className="text">
                    <p>Ingredients : </p>
                    <ul>
                        {(typeof ingredients !== 'undefined' && ingredients.length > 0 ? ingredients.map((valeur,index) => <li key={index}>{valeur}</li>) : null)}
                    </ul>
                </div>
            </div>
            <div className="method">
                <h2>Instructions :</h2>
                <p>{data.strInstructions}</p>
            </div>
        </div>
    </div>
    )
}