import React from "react";

import Link from "./Link";

const Header = () => {
  return (
    <div className="header">
      <Link href="/home" className="link">
        HomePage
      </Link>
      <Link href="/meal" className="link">
        Check the meal
      </Link>
      <Link href="/drink" className="link">
        Check the drink
      </Link>
      <Link href="/api-use" className="link">
        Api Use
      </Link>
    </div>
  );
};

export default Header;